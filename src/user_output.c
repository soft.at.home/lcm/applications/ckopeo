/****************************************************************************
**
** Copyright (c) 2020 SoftAtHome
**
** Redistribution and use in source and binary forms, with or
** without modification, are permitted provided that the following
** conditions are met:
**
** 1. Redistributions of source code must retain the above copyright
** notice, this list of conditions and the following disclaimer.
**
** 2. Redistributions in binary form must reproduce the above
** copyright notice, this list of conditions and the following
** disclaimer in the documentation and/or other materials provided
** with the distribution.
**
** Subject to the terms and conditions of this license, each
** copyright holder and contributor hereby grants to those receiving
** rights under this license a perpetual, worldwide, non-exclusive,
** no-charge, royalty-free, irrevocable (except for failure to
** satisfy the conditions of this license) patent license to make,
** have made, use, offer to sell, sell, import, and otherwise
** transfer this software, where such license applies only to those
** patent claims, already acquired or hereafter acquired, licensable
** by such copyright holder or contributor that are necessarily
** infringed by:
**
** (a) their Contribution(s) (the licensed copyrights of copyright
** holders and non-copyrightable additions of contributors, in
** source or binary form) alone; or
**
** (b) combination of their Contribution(s) with the work of
** authorship to which such Contribution(s) was added by such
** copyright holder or contributor, if, at the time the Contribution
** is added, such addition causes such combination to be necessarily
** infringed. The patent license shall not apply to any other
** combinations which include the Contribution.
**
** Except as expressly stated above, no rights or licenses from any
** copyright holder or contributor is granted under this license,
** whether expressly, by implication, estoppel or otherwise.
**
** DISCLAIMER
**
** THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
** CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES,
** INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
** MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
** DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR
** CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
** SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
** LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
** USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED
** AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
** LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
** ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
** POSSIBILITY OF SUCH DAMAGE.
**
****************************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>

#define UNUSED __attribute__((unused))

#include "main.h"

void ckopeo_print_usage_option(const char* so,
                               const char* lo,
                               const char* args,
                               const char* description) {
    int pre_length = 4 + 2 + 4 + strlen(lo);
    printf("    %s%s%s    %s%s%s",
           c(GREEN), so, c(RESET),
           c(GREEN), lo, c(RESET));
    if(args != NULL) {
        pre_length += 3 + strlen(args);
        printf(" %s<%s>%s",
               c(BLUE), args, c(RESET));
    }
    for(int i = 35 - pre_length; i > 0; i--) {
        printf(" ");
    }
    printf("%s%s%s\n",
           c(WHITE), description, c(RESET));
}

void ckopeo_print_notes(void) {
    printf("\n");
    printf("%sNotes:%s\n", c(CYAN), c(RESET));
    printf(" %s*%s Short and long options take the same arguments\n", c(YELLOW), c(RESET));
    printf("\n");
    printf(" %s*%s At least one odl file or odl string must be specified\n", c(YELLOW), c(RESET));
    printf("\n");
    printf(" %s*%s Each bus backend can only be specified once\n", c(YELLOW), c(RESET));
    printf("\n");
    printf(" %s*%s The process daemizes (-D) after the entry points are called, but before\n", c(YELLOW), c(RESET));
    printf("   the data model is registered to the busses.\n");
    printf("\n");
    printf(" %s*%s All command line options can be put in the config section of one\n", c(YELLOW), c(RESET));
    printf("   of the main odls.\n");
}

void ckopeo_print_config_example(void) {
    printf("\n");
    printf("%sExample config section:%s\n", c(CYAN), c(RESET));
    printf("\n");
    printf("%s%%config%s {\n", c(GREEN), c(RESET));
    printf("    %suris%s = %s\"pcb:/var/run/pcb_sys,ubus:/var/run/ubus/ubus.sock\"%s;\n", c(BLUE), c(RESET), c(CYAN), c(RESET));
    printf("    %sbackends%s = %s\"/usr/bin/mods/amxb/mod-amxb-pcb.so,/usr/bin/mods/amxb/mod-amxb-ubus.so\"%s;\n", c(BLUE), c(RESET), c(CYAN), c(RESET));
    printf("    %sauto-detect%s = %strue%s;\n", c(BLUE), c(RESET), c(CYAN), c(RESET));
    printf("    %sauto-connect%s = %strue%s;\n", c(BLUE), c(RESET), c(CYAN), c(RESET));
    printf("    %sinclude-dirs%s = %s\".\"%s;\n", c(BLUE), c(RESET), c(CYAN), c(RESET));
    printf("    %simport-dirs%s = %s\".\"%s;\n", c(BLUE), c(RESET), c(CYAN), c(RESET));
    printf("    %sdaemon%s = %sfalse%s;\n", c(BLUE), c(RESET), c(CYAN), c(RESET));
    printf("    %spriority%s = %s0%s;\n", c(BLUE), c(RESET), c(CYAN), c(RESET));
    printf("}\n");
}

void ckopeo_print_usage(UNUSED int argc, char* argv[]) {
    printf("%s [OPTIONS] <odl files>\n", argv[0]);

    printf("\n");
    printf("%sOptions%s:\n", c(CYAN), c(RESET));
    printf("\n");
    ckopeo_print_usage_option("-h", "--help", NULL, "Print usage help");
    ckopeo_print_usage_option("-H", "--HELP", NULL, "Print extended help");
    ckopeo_print_usage_option("-B", "--backend", "so file", "Loads the shared object as bus backend");
    ckopeo_print_usage_option("-u", "--uri", "uri", "Adds an uri to the list of uris");
    ckopeo_print_usage_option("-A", "--no-auto-detect", NULL, "Do not auto detect unix domain sockets and back-ends");
    ckopeo_print_usage_option("-C", "--no-connect", NULL, "Do not auto connect the provided or detected uris");
    ckopeo_print_usage_option("-I", "--include-dir", "dir", "Adds include directory for odl parser, multiple allowed");
    ckopeo_print_usage_option("-L", "--import-dir", "dir", "Adds import directory for odl parser, multiple allowed");
    ckopeo_print_usage_option("-o", "--option", "name=value", "Adds a configuration option");
    ckopeo_print_usage_option("-O", "--ODL", "<odl string>", "An ODL in string format, only one ODL string allowed");
    ckopeo_print_usage_option("-D", "--daemon", NULL, "Daemonize the process");
    ckopeo_print_usage_option("-p", "--priority", "nice level", "Sets the process nice level");
    ckopeo_print_usage_option("-N", "--no-pid-file", NULL, "Disables the creation of a pid-file in /var/run");
    ckopeo_print_usage_option("-E", "--eventing", NULL, "Enables eventing during loading of ODL files");
    ckopeo_print_usage_option("-d", "--dump-config", NULL, "Dumps configuration options at start-up");
    printf("\n");
}

void ckopeo_print_help(int argc, char* argv[]) {
    ckopeo_print_usage(argc, argv);
    ckopeo_print_notes();
    ckopeo_print_config_example();
}

void ckopeo_print_configuration(amxc_var_t* config) {
    fprintf(stderr, "\n%sConfiguration:%s\n", c(GREEN), c(RESET));
    fflush(stderr);
    amxc_var_dump(config, STDERR_FILENO);
}

void ckopeo_print_error(const char* fmt, ...) {
    va_list args;

    fprintf(stderr, "%sERROR%s -- %s", c(RED), c(RESET), c(WHITE));

    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);

    fprintf(stderr, "%s\n", c(RESET));
}

void ckopeo_print_message(const char* fmt, ...) {
    va_list args;

    fprintf(stderr, "%sINFO%s -- %s", c(YELLOW), c(RESET), c(WHITE));

    va_start(args, fmt);
    vfprintf(stderr, fmt, args);
    va_end(args);

    fprintf(stderr, "%s\n", c(RESET));
}

